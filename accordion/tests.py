from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.http import HttpRequest
from .views import index

# Create your tests here.
class story7UnitTest(TestCase):
    def test_url_response_code(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_view_dan_template(self):
        response = self.client.get('/')
        found = resolve('/')
        self.assertEqual(found.view_name, "accordion:index")
        self.assertTemplateUsed(response, 'story7.html')

    def test_isi_html(self):
        request = HttpRequest()
        response = index(request)
        accordion = response.content.decode('utf 8')
        self.assertIn("My profile", accordion)
        self.assertIn("What I'm doing these days", accordion)
        self.assertIn("What are these?", accordion)
        self.assertIn("Experience", accordion)

